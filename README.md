This is the java version of this file: http://www.inf.unideb.hu/~nbatfai/z3a7.cpp
This project is only for school purposes but feel free to modify it.

USAGE:
The class Csomopont is in the package p1 so first compile the Csomopont class:
javac -d . Csomopont.java
and then you can compile the LZWBinFaLaunch.java which contains the main method:
javac LZWBinFaLaunch.java
then just run:
java LZWBinFaLaunch input_file output_file