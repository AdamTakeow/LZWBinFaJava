// 	Bertalan Adam 2015 takeow.adam@gmail.com

// LZWBinTree.java

/*  Copyright (C) 2015  Bertalan Ádám

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	Version history:
	1.0.0 - Create the LZWBinFaLaunch class and added parameter check
	1.0.1 - Added possibility to read from file.
	1.0.2 - Added a better file reading
	1.0.3 - Changed output stream for flexibility
	1.0.4 - Changed Input stream to fix reading problems
	1.1.4 - Merge Node class with LZWBinFaLaunch
	1.2.4 - Merge ownLZWBinFa class and rename to LZWBinTree
	1.3.4 - Using ArrayList, collected the leaf elements and raise performance
	1.3.5 - Merge getVar and getMean methods into one calc method
	1.4.5 - Faster file reading in Launch.java
*/

import java.io.PrintStream;
import java.util.ArrayList;
// import java.util.Iterator;

public class LZWBinTree
{
	public LZWBinTree()
	{
		tree = root;
	}
	
	public void build( char b )
	{
		if( b == '0' )
		{
			if( tree.getLeftZero() == null )
			{
				Node uj = new Node( '0', tree );
				tree.setLeftZero( uj );
				tree = root;
			}
			else
			{
				tree = tree.getLeftZero();
			}
		}
		else
		{
			if( tree.getRightOne() == null )
			{
				Node uj = new Node( '1', tree );
				tree.setRightOne( uj );
				tree = root;
			}
			else
			{
				tree = tree.getRightOne();
			}	
		}
	}

	// if print called with no arguments, then it will print to the System.out
	public void print()
	{
		print( root, new PrintStream( System.out ) );
	}

	// if print called with a PrintStream object or one of its descendant then it will print there
	public void print( PrintStream os ) 
	{
		print( root, os );
	}

	// the print method get two arguments, a Node and a PrintStream object
	private void print( Node elem, PrintStream os ) 
	{
		// inorder
		if( elem != null )
		{
			// calculate the actual node's depth
			int depth = elem.getValue();
			
			// recursively call this method
			print( elem.getRightOne(), os );
		
				for( int i = 0; i < depth; ++i )
					os.print( "---" );
		
				os.print( elem.getChar() );
				os.print( "(" );
				os.print( depth );
				os.println( ")" );

			// recursively call this method
			print( elem.getLeftZero(), os );
		}
	}
	
	// this method calls the getResult method with an outputstream
	public void getResult()
	{
		getResult( new PrintStream( System.out ) );
	}
	
	// this method prints the depth, mean and the variance of the tree
	public void getResult( PrintStream os )
	{
		// first we collect the leaf elements into an array
		seekLeafElements( root );
		
		/* getMean() calculates the mean */
		
		double mean = 0.0;
		int meansum = 0;
		int meandb = 0;

		meandb = leafElements.size(); // number of leaf elements

		for( Node it : leafElements )
		{
			meansum += it.getValue();
		}
		
  		mean = ((double) meansum) / meandb;
		theMean = mean; // save the mean
		
		/* getVar(), calculates the variance */

  		double var = theMean; // the actual mean
		double varsum = 0.0;
		
		for( Node it : leafElements )
		{
			int depth = it.getValue();
			varsum += ((depth - theMean) * (depth - theMean));
		}

  		if( meandb - 1 > 0 )
    		var = Math.sqrt( varsum / (meandb - 1) );
  		else
    		var = Math.sqrt( varsum );
		
		theVar = var; // save the variance		
		
		// after building the tree, the Node class knows the max depth of the tree
		os.println( "depth: " + Node.depth() );
		// this order is important! first calc the mean and THEN the var
		os.println( "mean: " + theMean );// getMean() );
		os.println( "var: " + theVar ) ;// getVar() );
	}

	// this method collects the leaf elements into an array
	private void seekLeafElements( Node elem )
	{
		if( elem != null )
		{
			seekLeafElements( elem.getRightOne() );
			seekLeafElements( elem.getLeftZero() );
			
			if( elem.getRightOne() == null && elem.getLeftZero() == null )
			{
				leafElements.add( elem );
			}
		}
	}
	
	/* FIELDS */
	
	// store the mean and the variance
	private double theMean, theVar;
	
	// an array which stores the leaf elements of the tree
	private ArrayList<Node> leafElements = new ArrayList<Node>();
	
	// the actual Node reference
	private Node tree = null;
	
	// the root Node
	protected Node root = new Node( '/', null );
}

	/* NODE class */
class Node
{
	public Node( char b, Node p )
	{
		c = b; // (1) (0) (/)
		leftZero = null; // left child
		rightOne = null; // right child
		parent = p; 
		
		// calculate the actual depth
		if( parent != null )
			value = parent.getValue() + 1;
		else
			value = 0; // only if root element comes
		
		// calculate the max of depth
		if( value > depth )
		{
			depth = value;
		}
	}
	
	public Node getRightOne(){ return rightOne; }
	
	public Node getLeftZero(){ return leftZero; }
	
	public void setRightOne( Node gy ){ rightOne = gy; }
	
	public void setLeftZero( Node gy ){ leftZero = gy; }

	public char getChar(){ return c; }
	
	public static int depth(){ return depth; }
	
	public int getValue(){ return value; }
	
	public void setParent( Node gy ){ parent = gy; }
	
	public Node getParent(){ return parent; }
	
	private static int depth = 0;
	private char c;
	private int value;
	
	private Node leftZero = null;
	private Node rightOne = null; 
	private Node parent = null;
}

