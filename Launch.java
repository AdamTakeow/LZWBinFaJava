// 	Bertalan Adam 2015 takeow.adam@gmail.com

// LZWBinTree.java

/*  Copyright (C) 2015  Bertalan Ádám

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.io.Reader;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.FileInputStream;
import java.util.Scanner;

public class Launch
{
	public static void main( String args[] ) // throws IOException
	{
		// USAGE: 	javac Launch.java
		// 			java Launch infile -o outfile
		
		// in java the args does not contain the program name itself
		// so we need two args an input and an output file
		
		LZWBinTree tree = new LZWBinTree();
		
		// try-with-resources, thanks for drawing my attention to this feature Mate Toth 
		try (	Scanner input = new Scanner( new File( args[0] ) );
				PrintStream output = new PrintStream( new File( args[2] ) )
			)
		{
			// chech if there is the -o option
			if( args[1].charAt(0) != '-' || args[1].charAt(1) != 'o' )
			{
				throw new IOException( "Missing -o arg!" );
			}
			
			// we read the lines to the buf			
			String buf = "";
			
			boolean comment = false;
			
			// while there is any line
			while( input.hasNext() )
			{
				// we read the whole line
				buf = input.nextLine();
				// and add a newline to the end because nextLine wont read it
				buf += "\n";
				
				byte b[] = new byte[1];
				
				// we go through every char in the string
				for( int i = 0; i < buf.length(); ++i )
				{
					// convert it to byte
					b[0] = (byte)buf.charAt( i );
				
					// and do the rest checking
					if( b[0] == 0x3e ) // >
					{
						comment = true;
						continue;
					}
		
					if( b[0] == 0x0a ) // 0x0a newline
					{
						comment = false;
						continue;
					}
	
					if( comment )
					{
						continue;
					}
	
					if( b[0] == 0x4e ) // N
					{
						continue;
					}
	
					for (int j = 0; j < 8; ++j )
					{
	   					if ( ( b[0] & 0x80 ) != 0 ) // 10000000
	   					{
	       					tree.build( '1' );
	   					}
	   					else
	   					{
	       					tree.build( '0' );
	   					}
	   					b[0] <<= 1; // bitwise shift
	   				}
				}
			}
			
			// calls kiir with no parameter and it will print to STDOUT
			// tree.print(); // write to console
			
			// print the variance, mean and depth
			tree.getResult();

			// calls kiir with a PrintStream object so it will print to there
			// tree.kiir( output ); // write to file
			// print the variance, mean and depth
			// tree.getResult( output );
		}
		catch( Exception e )
		{
			System.out.println( e );
			System.out.println( "USAGE: java LZWBinFaLaunch infile -o outfile" );
			e.printStackTrace();
			System.exit( -1 );
		}
	}
}